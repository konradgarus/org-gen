package com.expium;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.crowd.exception.OperationNotPermittedException;
import com.atlassian.crowd.exception.embedded.InvalidGroupException;
import com.atlassian.jira.bc.group.search.GroupPickerSearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserDetails;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.servicedesk.api.ServiceDesk;
import com.atlassian.servicedesk.api.ServiceDeskService;
import com.atlassian.servicedesk.api.organization.CustomerOrganization;
import com.atlassian.servicedesk.api.organization.OrganizationService;
import com.atlassian.servicedesk.api.organization.OrganizationsQuery;
import com.atlassian.servicedesk.api.organization.UsersOrganizationUpdateParameters;
import com.atlassian.servicedesk.api.util.paging.PagedResponse;
import com.atlassian.servicedesk.api.util.paging.SimplePagedRequest;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Service
public class Generator {
    private static final int CONTACTS_PER_CLIENT = 1;

    private final GroupManager groupManager;
    private final GroupPickerSearchService groupPickerSearchService;
    private final OrganizationService organizationService;
    private final ServiceDeskService serviceDeskService;
    private final ProjectManager projectManager;
    private final UserManager userManager;

    @Inject
    public Generator(
            @ComponentImport GroupManager groupManager,
            @ComponentImport GroupPickerSearchService groupPickerSearchService,
            @ComponentImport OrganizationService organizationService,
            @ComponentImport ServiceDeskService serviceDeskService,
            @ComponentImport ProjectManager projectManager,
            @ComponentImport UserManager userManager) {
        this.groupManager = groupManager;
        this.groupPickerSearchService = groupPickerSearchService;
        this.organizationService = organizationService;
        this.serviceDeskService = serviceDeskService;
        this.projectManager = projectManager;
        this.userManager = userManager;
    }

    public void generate(int orgs, String jsdProjectKey) throws Exception {
        ApplicationUser user = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
        Project project = projectManager.getProjectObjByKey(jsdProjectKey);
        ServiceDesk serviceDesk = serviceDeskService.getServiceDeskForProject(user, project).right().get();
        Map<String, CustomerOrganization> organizations = findOrganizations(user, serviceDesk);
        Map<String, ApplicationUser> users = findUsers();
        Map<String, Group> groups = findGroups();

        for (int i = 0; i < orgs; i++) {
            String clientName = clientName(i);
            System.out.println("Generating: " + clientName);
            generateGroupIfNeeded(clientName, groups);
            List<ApplicationUser> clientContacts = createClientContacts(users, groups.get(clientName));
            CustomerOrganization org = generateOrganizationIfNeeded(user, organizations, clientName, serviceDesk);
            assignCustomersToOrganization(user, clientContacts, org);
        }
    }

    private HashMap<String, Group> findGroups() {
        return new HashMap<>(groupPickerSearchService.findGroups("").stream()
                .collect(Collectors.toConcurrentMap(g -> g.getName(), g -> g)));
    }

    private Map<String, CustomerOrganization> findOrganizations(ApplicationUser user, ServiceDesk serviceDesk) {
        Map<String, CustomerOrganization> result = new ConcurrentHashMap<>();
        PagedResponse<CustomerOrganization> resp;
        int start = 0;
        do {
            OrganizationsQuery query = organizationService.newOrganizationsQueryBuilder()
                    .serviceDeskId(serviceDesk.getId())
                    .pagedRequest(new SimplePagedRequest(start, 1000))
                    .build();
            resp = organizationService.getOrganizations(user, query).right().get();
            for (CustomerOrganization org : resp.getResults()) {
                result.put(org.getName(), org);
            }
            start += resp.size();
        } while (resp.hasNextPage());
        return result;
    }

    @SuppressWarnings("deprecation")
    private Map<String, ApplicationUser> findUsers() {
        return userManager.getUsers().stream().collect(Collectors.toConcurrentMap(u -> u.getUsername(), u -> u));
    }

    private static String clientName(int number) {
        return format("CLIENT_%06d", number);
    }

    private boolean generateGroupIfNeeded(String name, Map<String, Group> groups) throws OperationNotPermittedException,
            InvalidGroupException {
        if (!groups.containsKey(name)) {
            Group group = groupManager.createGroup(name);
            groups.put(name, group);
            return true;
        }
        return false;
    }

    private List<ApplicationUser> createClientContacts(Map<String, ApplicationUser> users, Group clientGroup) throws
            Exception {
        List<ApplicationUser> contacts = new ArrayList<>();
        for (int i = 1; i <= CONTACTS_PER_CLIENT; i++) {
            String username = clientGroup.getName().toLowerCase() + "_contact_" + format("%04d", i);
            ApplicationUser user = Optional.ofNullable(users.get(username)).orElseGet(() -> {
                try {
                    ApplicationUser newUser = null;
                    return userManager.createUser(new UserDetails(username, username));
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            });
            assignToGroup(user, clientGroup);
            contacts.add(user);
        }
        return contacts;
    }

    private void assignToGroup(ApplicationUser user, Group group) throws Exception {
        groupManager.addUserToGroup(user, group);
    }

    private CustomerOrganization generateOrganizationIfNeeded(
            ApplicationUser user,
            Map<String, CustomerOrganization> organizations,
            String name,
            ServiceDesk serviceDesk
    ) {
        if (!organizations.containsKey(name)) {
            organizationService
                    .createOrganization(user, organizationService.newCreateBuilder().name(name).build())
                    .map(org -> {
                        organizations.put(name, org);
                        return organizationService.addOrganizationToServiceDesk(user,
                                organizationService
                                        .newOrganizationServiceDeskUpdateParametersBuilder()
                                        .serviceDeskId(serviceDesk.getId())
                                        .organization(org)
                                        .build());
                    })
                    .left().forEach(err -> System.out.println("ERROR: " + err));
        }
        return organizations.get(name);
    }

    private void assignCustomersToOrganization(ApplicationUser user, List<ApplicationUser> clientContacts,
                                               CustomerOrganization org) {
        UsersOrganizationUpdateParameters update = organizationService.newUsersOrganizationUpdateParametersBuilder()
                .organization(org).users(new HashSet<>(clientContacts)).build();
        organizationService.addUsersToOrganization(user, update);
    }
}

package com.expium.api;

import com.expium.Generator;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

@Path("/generate")
public class Resource {

    private final Generator generator;

    public Resource(Generator generator) {
        this.generator = generator;
    }

    @GET
    @Produces("text/plain")
    public String generate(
            @QueryParam("n") int n,
            @QueryParam("project") String project
    ) throws Exception {
        generator.generate(n, project);
        return "ok";
    }
}
